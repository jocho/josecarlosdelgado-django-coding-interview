from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    pass

class Student(User):
    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'

    student_id = models.IntegerField(null=True)
    enrolled_completed_flag = models.BooleanField(default=False)

    def save(self, *args, **kwargs):
        if self.student_id and not self.username:
            self.username = self.student_id
        super().save(*args, **kwargs)
