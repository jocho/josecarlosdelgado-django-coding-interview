from django.urls import path, include
from rest_framework.routers import DefaultRouter

from student import views

# Create a router and register our ViewSets with it.
router = DefaultRouter()
router.register(r'students', views.StudentViewSet, basename='student')

# The API URLs are now determined automatically by the router.
urlpatterns = [
    path('', include(router.urls)),
]